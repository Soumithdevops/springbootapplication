package com.example.app;

import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=EurekaServer1Application.class , webEnvironment=WebEnvironment.DEFINED_PORT)
@Profile("local")
public class EurekaServer1ApplicationTests {

	TestRestTemplate restTemplate=new TestRestTemplate();
	
	@LocalServerPort
	  int randomPort;
	
	
	
	
	@Test
	public void testGetEmployeesList() {
		String endpointUrl="http://localhost:"+randomPort+"/getEmployeesList";
		System.out.println(endpointUrl);
		HttpHeaders headers=new HttpHeaders();
		HttpEntity<String> entity=new HttpEntity<String>(headers);
		ResponseEntity<List<EmployeeTable>> response=restTemplate.exchange(endpointUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<EmployeeTable>>(){});
        assertNotNull(response);		
	}
	
	
	/*
	 * @Test public void testCreateEmployee() { String
	 * endpointUrl="http://localhost:8080/createEmployee"; HttpHeaders headers=new
	 * HttpHeaders(); headers.setContentType(MediaType.APPLICATION_JSON);
	 * headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	 * EmployeeTable table=new EmployeeTable(); table.setFirstName("Raghav");
	 * table.setLastName("Raghav -last"); table.setPhoneNum("9654654465");
	 * HttpEntity<EmployeeTable> entity=new
	 * HttpEntity<EmployeeTable>(table,headers); ResponseEntity<String>
	 * response=restTemplate.exchange(endpointUrl, HttpMethod.POST, entity,
	 * String.class); System.out.println(response.getBody());
	 * 
	 * 
	 * }
	 */
	 
}
