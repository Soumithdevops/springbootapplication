package com.example.app;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
public class EurekaServer1ControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	EmployeeRepository repository;
	
	@Test
	public void testGetEmployeesList() {
		List<EmployeeTable> list=new ArrayList<>();
		EmployeeTable emp=new EmployeeTable();
		emp.setId(1);
		emp.setFirstName("ramu");
		emp.setLastName("Rama");
		
		EmployeeTable emp1=new EmployeeTable();
		emp1.setId(1);
		emp1.setFirstName("ramu1");
		emp1.setLastName("Rama1");
		list.add(emp1);
		list.add(emp);
		
		when(repository.findAll()).thenReturn(list);
		
		try {
			MvcResult result=mockMvc.perform(get("/getEmployeesList").
					accept(MediaType.APPLICATION_JSON).
					contentType(MediaType.APPLICATION_JSON)).andReturn();
			
			
			ObjectMapper mapper = new ObjectMapper();

			// this uses a TypeReference to inform Jackson about the Lists's generic type
			List<EmployeeTable> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EmployeeTable>>() {});
			System.out.println(result.getResponse().getContentAsString());
			assertEquals(list.size(), actual.size());
			//System.out.println();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
