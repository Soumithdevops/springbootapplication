package com.example.app;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeRepository repository;

	@GetMapping("/getEmployeesList")
	public List<EmployeeTable> getAllEmployees() {
		return repository.findAll();
	}

	@GetMapping("/getEmployeesListByFirstName")
	public List<EmployeeTable> getEmployeesByName(@RequestParam String firstName) {
		return repository.getEmployeesByFirstName(firstName);
	}

	@PostMapping("/createEmployee")
	public String getEmployeesByName(@RequestBody EmployeeTable empRequest) {
		repository.save(empRequest);
		return "success";
	}
	
	
	@PutMapping("/updateEmployee")
	public String updateExistingEmployee(@RequestBody EmployeeTable empRequest) {
		
		Optional<EmployeeTable> empExistingObj=repository.findById(empRequest.getId());
		if(empExistingObj.isPresent()){
			repository.save(empRequest);
		}else {
			return "no employee found with given id "+empRequest.getId();
		}
		
		return "success";
	}
}
