package com.example.app;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeTable, Integer> {
	
	
 @Query("select emp from EmployeeTable emp where emp.firstName=:firstName")
  public List<EmployeeTable> getEmployeesByFirstName(@Param(value = "firstName") String firstName);
}
